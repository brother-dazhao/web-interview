# web-interview

<p align="left">
  <a href="https://github.com/yisainan/web-interview/issues"><img src="https://img.shields.io/github/issues/yisainan/web-interview" alt="issues"></a>
  <a href="https://github.com/yisainan/web-interview/stargazers"><img src="https://img.shields.io/github/stars/yisainan/web-interview" alt="stars"></a>
  <a href="https://github.com/yisainan/web-interview/network/members"><img src="https://img.shields.io/github/forks/yisainan/web-interview" alt="forks"></a>  
</p>

## 目录

<b><details><summary>💡 介绍</summary></b>

1、本仓库是面向 web 前端开发者准备面试使用；知识在于积累，切勿刷题作面霸！

2、如何成为一名合格的前端开发工程师呢？

首先前端基础一定要精（三大基础：html, css, js），其他基于这些扩展出来的框架（三大框架：vue，react，angular）一定要广。底层精，上层广，这就是一名合格的前端开发工程师。

3、建议阅读

- [写给前端面试者](https://github.com/amfe/article/issues/5)

🙏 建议自己先有个思考的过程，有了自己的答案或者疑问再看解析进行对比；仓库将持续更新，欢迎 <b>Star</b>，如有内容错误或改进意见，欢迎 [issue](https://github.com/yisainan/web-interview/issues/new?assignees=&labels=Bug&template=---------.md&title=%5Bbug%5D+) 或 pr。

</details>

<b><details><summary>🐭 HTML</summary></b>

- [详情](./content/HTML.md)
- [浏览器](./content/浏览器.md)

</details>

<b><details><summary>🐮 CSS</summary></b>

- [详情](./content/CSS.md)

</details>

<b><details><summary>🐯 JS</summary></b>

- [js 基础](./content/js/js.md)
- [es6](./content/js/es6.md)
- [jquery](./content/js/jquery.md)
- [node](./content/js/node.md)
- [Ajax](./content/js/Ajax.md)
- [算法](./content/js/算法.md)

</details>

<b><details><summary>🐰 Vue + React</summary></b>

- Vue
  - [Vue](./content/vue/vue.md)
  - [fe-interview-vue](./content/vue/fe-interview-vue.md)

- React
  - [React](./content/react/React.md)
  - [reactjs-interview-questions](./content/react/reactjs-interview-questions.md)
  - [fe-interview-react](./content/react/fe-interview-react.md)

</details>

<b><details><summary>🐉 Angular</summary></b>

- [详情](./content/Angular.md)

</details>

<b><details><summary>🐍 微信小程序</summary></b>

- [详情](./content/微信小程序.md)

</details>

<b><details><summary>🐎 选择题</summary></b>

- [详情](./content/选择题/js.md)
- [JavaScript专项练习](./content/选择题/JavaScript专项练习.md)

</details>

<b><details><summary>🐐 编程题</summary></b>

- [js 基础](./content/编程题/js.md)
- [js 原型](./content/编程题/prototype.md)
- [es6](./content/编程题/es6.md)
- [变量提升](./content/编程题/变量提升.md)

</details>

<b><details><summary>🙊 兼容性问题</summary></b>

- [详情](./content/兼容性问题.md)

</details>

<b><details><summary>🐔 大厂高频题</summary></b>

- [详情](./content/大厂高频题.md)

</details>

<b><details><summary>🐶 网络及安全防护</summary></b>

- [详情](./content/网络及安全防护.md)

</details>

<b><details><summary>🐷 框架/工程/项目</summary></b>

- [详情](./content/框架工程项目.md)

</details>

<b><details><summary>❓ 软技能</summary></b>

- [常问的非技术问题](./content/软技能/非技术问题.md)
- [面试技巧及注意事项](./content/软技能/面试技巧及注意事项.md)
- [谈钱不伤感情，如何在面试中争取高薪](./content/软技能/谈钱不伤感情，如何在面试中争取高薪.md)

</details>

<b><details><summary>📝 前端简历</summary></b>

- [如何写好前端简历](./content/简历/如何写好前端简历.md)
- [获取简历模板](./content/简历/获取简历模板.md)

</details>

<b><details><summary>👬 贡献者</summary></b>

[按照本仓库收集时间排序，如有侵权请联系删除](https://github.com/yisainan/web-interview/projects/3#column-6811772)

</details>

<b><details><summary>📜 License</summary></b>

本仓库遵循 MIT 协议，转载请注明出处。

[![MIT](https://img.shields.io/github/license/yisainan/web-interview)](https://github.com/yisainan/web-interview/blob/master/LICENSE)

</details>